package JUnit;

public class Calculator {

    CalculatorService service;

    public Calculator(CalculatorService service){
        this.service = service;
    }

    public int perform(int x, int y){     //2 3 -> (i+j)*i
        return service.add(x, y)*x;
        //return (x+y)*x;
    }
}
