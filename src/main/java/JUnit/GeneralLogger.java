package JUnit;

public class GeneralLogger extends AbstractLogger {

    /**
     * Konstruktor von GeneralLogger
     *
     * @param name  Name des GeneralLoggers
     */
    public GeneralLogger(String name) {
        this.loggerName = name;
    }

    @Override
    public String logMessage(int level, String message) {
        write("Could not find Logger with Level " + level + ". Using GeneralLogger");
        write(message);
        return this.getLoggerName();
    }
}
