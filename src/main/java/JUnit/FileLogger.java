package JUnit;

public class FileLogger extends AbstractLogger {

    /**
     * Konstruktor des FileLoggers
     *
     * @param name  Name des FileLoggers
     * @param level Level des FileLoggers
     */
    public FileLogger(String name, int level) {
        this.loggerName = name;
        this.level = level;
    }

}
