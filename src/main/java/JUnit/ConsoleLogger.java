package JUnit;

public class ConsoleLogger extends AbstractLogger {

    /**
     * Konstruktor von ConsoleLogger
     *
     * @param name  Name des ConsoleLoggers
     * @param level Level des ConsoleLoggers
     */
    public ConsoleLogger(String name, int level) {
        this.loggerName = name;
        this.level = level;
    }

}
