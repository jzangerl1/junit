package JUnit;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Alle Logger erben von dieser Klasse
 */
public abstract class AbstractLogger {
    protected int level;
    protected String loggerName;
    private AbstractLogger nextLogger;

    /**
     * Setzt den Logger der nach dem jetzigem kommt
     *
     * @param nextLogger Der Logger der als nächstes kommt
     */
    public void setNextLogger(AbstractLogger nextLogger) {

        this.nextLogger = nextLogger;
    }

    public AbstractLogger getNextlogger(){
        return this.nextLogger;
    }

    /**
     * Gibt den Namen des Loggers zurück
     *
     * @return Der Name als String
     */
    public String getLoggerName() {

        return this.loggerName;
    }

    /**
     * Überprüft, ob der richtige Logger benutzt wird, wenn nicht, dann schick es an den nächsten Logger
     *
     * @param level   Das Level des Loggers, der es bearbeiten soll
     * @param message Die Nachricht für den Logger
     */
    public String logMessage(int level, String message) {
        String returnMessage ="";
        if (this.level == level) {
            write(message);
            return this.getLoggerName();
        } else {
            if (this.nextLogger != null) {
                write("Forwarding this to " + nextLogger.getLoggerName());
                returnMessage = nextLogger.logMessage(level, message);
            } else {
                write("The loglevel doesn't exist!");
            }

        }
        return returnMessage;
    }

    /**
     * Gibt die Nachricht aus
     *
     * @param message die Nachricht
     */
    public void write(String message) {
        String output = getLoggerName() + ": " + message;
        System.out.println(output);
        writeToFile(output);
    }


    /**
     * Schreibt die Nachricht in ein log File
     *
     * @param message die Nachricht, die hineingeschrieben werden soll
     */
    public void writeToFile(String message) {
        try {
            FileWriter fileWriter = new FileWriter("C:\\Users\\Jan\\Desktop\\IT-Kolleg\\3.Semester\\POSLAND\\Chain-of-Responsibility\\" + getLoggerName() + ".log", true);
            fileWriter.write(message + "\n");
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


