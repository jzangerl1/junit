package JUnit;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestTicketautomat {
    private Ticketautomat ticketautomat;

    @Before
    public void setUp(){
        ticketautomat = new Ticketautomat(300);
    }

    @Test
    public void testGibPreis(){
        assertEquals(300, ticketautomat.gibPreis());
    }

    @Test
    public void testGeldEinwerfen(){
        ticketautomat.geldEinwerfen(50);
        assertEquals(50, ticketautomat.gibBisherGezahltenBetrag());
        ticketautomat.geldEinwerfen(-1);
        assertEquals(50, ticketautomat.gibBisherGezahltenBetrag());
    }

    @Test
    public void testTicketDrucken(){
        //Zu wenig Geld
        ticketautomat.geldEinwerfen(50);
        ticketautomat.ticketDrucken();
        assertEquals(50, ticketautomat.gibBisherGezahltenBetrag());
        assertEquals(0, ticketautomat.gibGesamtsumme());
        //Genauer Betrag
        ticketautomat.geldEinwerfen(250);
        ticketautomat.ticketDrucken();
        assertEquals(0, ticketautomat.gibBisherGezahltenBetrag());
        assertEquals(300, ticketautomat.gibGesamtsumme());
        //Zu viel Geld
        ticketautomat.geldEinwerfen(400);
        ticketautomat.ticketDrucken();
        assertEquals(100, ticketautomat.gibBisherGezahltenBetrag());
        assertEquals(600, ticketautomat.gibGesamtsumme());
    }

    @Test
    public void testWechselgeldAuszahlen(){
        //Zu wenig Geld
        ticketautomat.geldEinwerfen(50);
        ticketautomat.ticketDrucken();
        assertEquals(50, ticketautomat.wechselgeldAuszahlen());
        //Genauer Betrag
        ticketautomat.geldEinwerfen(300);
        ticketautomat.ticketDrucken();
        assertEquals(0, ticketautomat.wechselgeldAuszahlen());
        //Zu viel Geld
        ticketautomat.geldEinwerfen(400);
        ticketautomat.ticketDrucken();
        assertEquals(100, ticketautomat.wechselgeldAuszahlen());
    }

    @Test
    public void testEntleeren(){
        //Zu wenig Geld
        ticketautomat.geldEinwerfen(50);
        ticketautomat.ticketDrucken();
        assertEquals(0, ticketautomat.gibGesamtsumme());
        //Genauer Betrag
        ticketautomat.geldEinwerfen(250);
        ticketautomat.ticketDrucken();
        assertEquals(300, ticketautomat.gibGesamtsumme());
        //Zu viel Geld
        ticketautomat.geldEinwerfen(400);
        ticketautomat.ticketDrucken();
        assertEquals(600, ticketautomat.gibGesamtsumme());
        //Entleeren
        ticketautomat.entleeren();
        assertEquals(0, ticketautomat.gibGesamtsumme());
    }
}
