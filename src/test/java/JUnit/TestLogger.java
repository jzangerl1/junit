package JUnit;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestLogger {
    private AbstractLogger errorLogger;
    private AbstractLogger consoleLogger;
    private AbstractLogger fileLogger;
    private AbstractLogger generalLogger;

    @Before
    public void setUp(){
        errorLogger = new ErrorLogger("Error1", 1);
        consoleLogger = new ConsoleLogger("Console1", 2);
        fileLogger = new FileLogger("File1", 3);
        generalLogger = new GeneralLogger("General1");
        errorLogger.setNextLogger(consoleLogger);
        consoleLogger.setNextLogger(fileLogger);
        fileLogger.setNextLogger(generalLogger);
    }

    @Test
    public void testNextLogger(){
        assertEquals(consoleLogger, errorLogger.getNextlogger());
        assertNotEquals(consoleLogger, fileLogger.getNextlogger());
        assertEquals(null, generalLogger.getNextlogger());
    }

    @Test
    public void testGetLoggerName(){
        assertEquals("Error1", errorLogger.getLoggerName());
    }

    @Test
    public void testLogMessage(){
        assertEquals("Error1", errorLogger.logMessage(1, "BLABLA"));
        assertNotEquals("File1", errorLogger.logMessage(2, "BLABLA"));
        assertEquals("General1", errorLogger.logMessage(500, "BALBAL"));

    }
}
